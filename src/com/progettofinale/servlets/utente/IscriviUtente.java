package com.progettofinale.servlets.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.dao.UtenteDao;
import com.progettofinale.model.ResponsoOperazione;
import com.progettofinale.model.Utente;

/**
 * Servlet implementation class IscriviUtente
 */
@WebServlet("/iscriviutente")
public class IscriviUtente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String varUsername = request.getParameter("input-user");
		System.out.println(varUsername);
		String varPassword = request.getParameter("input-psw");
		String varRuolo= request.getParameter("input-ruolo");
		String varIndirizzo = request.getParameter("input-indirizzo");
		
		Utente uTemp = new Utente();
		uTemp.setUsernameU(varUsername);
		uTemp.setPswU(varPassword);
		uTemp.setTipologiaU(varRuolo);
		uTemp.setIndirizzo(varIndirizzo);
		String strErrore = "";
		UtenteDao uDao = new UtenteDao();
		try {
			uDao.insert(uTemp);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			strErrore = e.getMessage();
		}
		
		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();
		
		
		
		if(uTemp.getIdUtente()== null) {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
			
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
		}
		
		
		
		
	}

}
