package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;

/**
 * Servlet implementation class DeleteStore
 */
@WebServlet("/DeleteStore")
public class DeleteStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteStore() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		PrintWriter out=response.getWriter();
		Integer identificatore = request.getParameter("idStore") != null ? Integer.parseInt(request.getParameter("idStore")) : -1;
		StoreDao sdao=new StoreDao();
		Store temp =new Store();
		temp.setIdStore(identificatore);
		
		  try {
		   if(sdao.delete(temp)) {
		    out.print(true);
		   }
		   else {
		    out.print(false);
		   }
		  } catch (SQLException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		  }
		 }
}
