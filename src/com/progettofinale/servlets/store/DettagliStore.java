package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Prodotto;
import com.progettofinale.model.Store;

/**
 * Servlet implementation class DettagliStore
 */
@WebServlet("/dettaglistore")
public class DettagliStore extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		Integer idStore = Integer.parseInt(request.getParameter("idStore"));
		Store sTemp = new Store();
		StoreDao sDao = new StoreDao();
		try {
			sTemp = sDao.getById(idStore);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ArrayList<Prodotto> elencoProdottiNelloStore = new ArrayList<Prodotto>();
		elencoProdottiNelloStore = sTemp.getElencoProdotti();
		
		String ret="";
		for(int i=0; i<elencoProdottiNelloStore.size();i++) {
			ret+=elencoProdottiNelloStore.get(i).getNomeP()+" "+elencoProdottiNelloStore.get(i).getPrezzo();
		}
		
		HttpSession sessione = request.getSession();
		sessione.setAttribute("nome", sTemp.getNomeS());
		sessione.setAttribute("indirizzo", sTemp.getIndirizzo());
		
		
		
		sessione.setAttribute("listaProdotti", ret);
		response.sendRedirect("dettagliStore.jsp");
		
		
		
		
		
		
		
		
	}

}
