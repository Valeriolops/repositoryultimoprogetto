package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;

/**
 * Servlet implementation class UpdateStore
 */
@WebServlet("/UpdateStore")
public class UpdateStore extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStore() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nomeS =request.getParameter("inputNome");
		String indirizzo=request.getParameter("inputIndirizzo");

		  Store temp=new Store();
		  temp.setNomeS(nomeS);
		  temp.setIndirizzo(indirizzo);
		  
		  StoreDao sdao=new StoreDao();
		  PrintWriter out=response.getWriter();
		  
		  try {
		   Store tempAgg=sdao.update(temp);
		   if((Integer) tempAgg.getIdStore()!=null) {
		    out.print(true);
		   }
		   else {
		    out.print(false);
		   }
		  } catch (SQLException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		  }
		 }

}
