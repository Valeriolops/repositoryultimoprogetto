package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;
/**
 * Servlet implementation class InsertStore
 */
@WebServlet("/InsertStore")
public class InsertStore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InsertStore() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		String nomeS = request.getParameter("input_nomeS") != null ? request.getParameter("input_nomeS") : "";
		String indirizzo = request.getParameter("input_indirizzo") != null ? request.getParameter("input_indirizzo") : null;
		
		StoreDao stDao = new StoreDao();
		Store st=new Store();
		st.setNomeS(nomeS);
		st.setIndirizzo(indirizzo);
		PrintWriter out=response.getWriter();
		  
		try {
			   stDao.insert(st);

			  if(st.getIdStore()>0) {
			   out.print(true); 
			  }
			  else {
			   out.print(false);
			  }

		}catch(SQLException e)
	{

		System.out.println("Errore in inserimento messaggio: " + e.getMessage());
	}

}

}
