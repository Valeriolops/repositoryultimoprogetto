package com.progettofinale.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettifinale.connessione.Connector;
import com.progettofinale.model.Utente;

public class UtenteDao implements Dao<Utente> {

	@Override
	public Utente getById(Integer id) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idUtente, usernameU, paswU, tipologiaU, indirizzo FROM Utente WHERE idUtente = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet result = ps.executeQuery();
		result.next();

		Utente temp = new Utente();

		temp.setIdUtente(result.getInt(1));
		temp.setUsernameU(result.getString(2));
		temp.setPswU(result.getString(3));
		temp.setTipologiaU(result.getString(4));
		temp.setIndirizzo(result.getString(5));

		return temp;
	}

	public Utente getByUsername(String username) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idUtente, usernameU, paswU, tipologiaU, indirizzo FROM Utente WHERE usernameU = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, username);
		ResultSet result = ps.executeQuery();
		result.next();

		Utente temp = new Utente();

		temp.setIdUtente(result.getInt(1));
		temp.setUsernameU(result.getString(2));
		temp.setPswU(result.getString(3));
		temp.setTipologiaU(result.getString(4));
		temp.setIndirizzo(result.getString(5));

		return temp;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		ArrayList<Utente> elencoUtenti = new ArrayList<Utente>();
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idUtente, usernameU, paswU, tipologiaU, indirizzo FROM Utente";
		PreparedStatement ps = conn.prepareStatement(query);

		ResultSet result = ps.executeQuery();
		while (result.next()) {
			Utente temp = new Utente();
			temp.setIdUtente(result.getInt(1));
			temp.setUsernameU(result.getString(2));
			temp.setPswU(result.getString(3));
			temp.setTipologiaU(result.getString(4));
			temp.setIndirizzo(result.getString(5));

			elencoUtenti.add(temp);
		}

		return elencoUtenti;
	}

	@Override
	public void insert(Utente t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "INSERT INTO Utente (usernameU, paswU, tipologiaU, indirizzo) VALUES (?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getUsernameU());
		ps.setString(2, t.getPswU());
		ps.setString(3, t.getTipologiaU());
		ps.setString(4, t.getIndirizzo());

		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();

		t.setIdUtente(result.getInt(1));
	}

	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		int id = this.getByUsername(t.getUsernameU()).getIdUtente();
		String query = "DELETE FROM utente WHERE idUtente = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);

		int deletedRows = ps.executeUpdate();

		if (deletedRows > 0)
			return true;
		return false;
	}

	@Override
	public Utente update(Utente t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE Utente SET usernameU = ?, paswU = ?, indirizzo = ? WHERE idUtente = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, t.getUsernameU());
		ps.setString(2, t.getPswU());
		ps.setString(3, t.getIndirizzo());
		ps.setInt(4, t.getIdUtente());

		int updatedRows = ps.executeUpdate();

		if (updatedRows > 0)
			return this.getById(t.getIdUtente());
		return null;
	}

}
