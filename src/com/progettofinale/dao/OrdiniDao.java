package com.progettofinale.dao;

import java.sql.Connection;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettifinale.connessione.Connector;
import com.progettofinale.model.Ordine;
import com.progettofinale.model.Prodotto;
import com.progettofinale.model.Store;

public class OrdiniDao implements Dao<Ordine> {

	@Override
	public Ordine getById(Integer idU) throws SQLException {

		Ordine order = new Ordine();
		ArrayList<Prodotto> elencoP = new ArrayList<Prodotto>();
		Prodotto pr = new Prodotto();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idUtente,quantiProdotto,dataUP,idProdotto, codiceP,nomeP,prezzoP FROM Utente"
				+ "    JOIN UtenteProdotto ON Utente.idUtente = UtenteProdotto.codiceUtente"
				+ "    JOIN Prodotto ON UtenteProdotto.codiceProdotto = Prodotto.idProdotto where idUtente=?;";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, idU);

		ResultSet result = ps.executeQuery();
		while (result.next()) {

			order.setCodiceUtente(result.getInt(1));
			order.setDataOrdine(result.getDate(3));
			pr.setIdProdotto(result.getInt(4));
			pr.setQuantitaDisponibile(result.getInt(2));
			pr.setNomeP(result.getString(6));
			pr.setCodiceP(result.getString(5));
			pr.setPrezzo(result.getDouble(7));
			elencoP.add(pr);

		}
		double tot = 0;
		for (int i = 0; i < elencoP.size(); i++) {
			tot += (elencoP.get(i).getPrezzo()) * (elencoP.get(i).getQuantitaDisponibile());
		}
		order.setTotale(tot);
		order.setElencoProdotti(elencoP);
		return order;
	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {

		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
		Ordine order = new Ordine();
		Prodotto pr = new Prodotto();

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idUtente,quantiProdotto,dataUP,idProdotto, codiceP,nomeP,prezzoP FROM Utente"
				+ "    JOIN UtenteProdotto ON Utente.idUtente = UtenteProdotto.codiceUtente"
				+ "    JOIN Prodotto ON UtenteProdotto.codiceProdotto = Prodotto.idProdotto;";
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();
		while (result.next()) {

			order.setCodiceUtente(result.getInt(1));
			order.setDataOrdine(result.getDate(3));
			pr.setIdProdotto(result.getInt(4));
			pr.setQuantitaDisponibile(result.getInt(2));
			pr.setNomeP(result.getString(6));
			pr.setCodiceP(result.getString(5));
			pr.setPrezzo(result.getDouble(7));

			if (elenco.size() > 0) {
				for (int i = 0; i < elenco.size(); i++) {
					if (elenco.get(i).getCodiceUtente() == order.getCodiceUtente()) {
						elenco.get(i).addProdotto(pr);
					} else {
						order.addProdotto(pr);
						elenco.add(order);
					}
				}

			} else {
				order.addProdotto(pr);
				elenco.add(order);
			}
		}
		for (int i = 0; i < elenco.size(); i++) {
			double tot = 0;
			for (int k = 0; k < (elenco.get(i).getElencoProdotti()).size(); k++) {

				tot += ((elenco.get(i).getElencoProdotti()).get(k).getPrezzo())
						* ((elenco.get(i).getElencoProdotti()).get(k).getQuantitaDisponibile());
			}
			elenco.get(i).setTotale(tot);
		}
		return elenco;
	}

	@Override
	public void insert(Ordine t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "INSERT INTO UtenteProdotto (codiceUtente,codiceProdotto,quantiProdotto,prezzoUP,dataUP) VALUES (?,?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query);

		ps.setInt(1, t.getCodiceUtente());
		ps.setDate(5, (Date) t.getDataOrdine());

		for (int i = 0; i < t.getElencoProdotti().size(); i++) {

			ps.setInt(2, t.getElencoProdotti().get(i).getIdProdotto());
			ps.setDouble(3, t.getElencoProdotti().get(i).getQuantitaDisponibile());
			ps.setDouble(4, t.getElencoProdotti().get(i).getPrezzo());
			ps.executeQuery();

		}

	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		int id = t.getCodiceUtente();
		String query = "DELETE FROM UtenteProdotto WHERE codiceUtente = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);

		int deletedRows = ps.executeUpdate();

		if (deletedRows > 0)
			return true;
		return false;
	}

	@Override
	public Ordine update(Ordine t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE UtenteProdotto SET quantiProdotto = ? WHERE CodiceUtente = ? && codiceProdotto=?;";
		PreparedStatement ps = conn.prepareStatement(query);

		ps.setInt(2, t.getCodiceUtente());
		
		for (int i = 0; i < t.getElencoProdotti().size(); i++) {
			ps.setInt(1, t.getElencoProdotti().get(i).getQuantitaDisponibile());
			ps.setInt(3, t.getElencoProdotti().get(i).getIdProdotto());
		}
		int updatedRows = ps.executeUpdate();

		if (updatedRows > 0)
			return t;
		return null;
	}

}
