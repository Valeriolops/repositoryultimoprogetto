package com.progettofinale.model;

public class Utente {
	
	
	private Integer idUtente;
	private String usernameU;
	private String pswU;
	private String tipologiaU;
	private String indirizzo;
	
	public Utente() {
		
	}

	public Integer getIdUtente() {
		return idUtente;
	}

	public void setIdUtente(Integer idUtente) {
		this.idUtente = idUtente;
	}

	public String getUsernameU() {
		return usernameU;
	}

	public void setUsernameU(String usernameU) {
		this.usernameU = usernameU;
	}

	public String getPswU() {
		return pswU;
	}

	public void setPswU(String pswU) {
		this.pswU = pswU;
	}

	public String getTipologiaU() {
		return tipologiaU;
	}

	public void setTipologiaU(String tipologiaU) {
		this.tipologiaU = tipologiaU;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	
	public String toStringHtmlTableEdit() {
		String  risultato = "<tr>";
					risultato+= "<td>"+this.idUtente +"</d>";
					risultato += "<td>" + this.usernameU + "</td>";
					risultato += "<td>" + this.pswU + "</td>";
					risultato += "<td>" + this.indirizzo + "</td>";
					
					risultato += "<td><a href='eliminautenteadmin?idUtente=" + this.idUtente + "'><i class=\"fas fa-trash\"></i></a></td>";	//PER LA GET!
					risultato += "<td><a href='modificautente.jsp?idUtente=" + this.idUtente + "'><i class=\"fas fa-pencil\"></i></a></td>";	//PER LA GET!

	
	return risultato;
	
	
	}
	
	
	
	

}
