package com.progettofinale.model;

import java.util.ArrayList;
import java.util.Date;

public class Ordine {
	private Integer codiceUtente;
	private double totale;
	private Date dataOrdine;
	private ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();
	
	public Ordine() {
		
	}

	public Integer getCodiceUtente() {
		return codiceUtente;
	}

	public void setCodiceUtente(Integer codiceUtente) {
		this.codiceUtente = codiceUtente;
	}

	public double getTotale() {
		return totale;
	}

	public void setTotale(double totale) {
		this.totale = totale;
	}

	public Date getDataOrdine() {
		return dataOrdine;
	}

	public void setDataOrdine(Date dataOrdine) {
		this.dataOrdine = dataOrdine;
	}

	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}
	
	public void addProdotto(Prodotto prodotto) {
		this.elencoProdotti.add(prodotto);
	}

	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}

	@Override
	public String toString() {
		return "Ordine [codiceUtente=" + codiceUtente + ", totale=" + totale + ", dataOrdine=" + dataOrdine
				+ ", elencoProdotti=" + elencoProdotti + "]";
	}
	
}
