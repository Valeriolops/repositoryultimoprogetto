package com.progettofinale.model;

public class Prodotto {
	private Integer idProdotto;
	private String codiceP;
	private String nomeP;
	private double prezzo;
	private Integer quantitaDisponibile;
	
	public Prodotto(){
		
	}

	
	
	public Integer getQuantitaDisponibile() {
		return quantitaDisponibile;
	}



	public void setQuantitaDisponibile(Integer quantitaDisponibile) {
		this.quantitaDisponibile = quantitaDisponibile;
	}



	public Integer getIdProdotto() {
		return idProdotto;
	}

	public void setIdProdotto(Integer idProdotto) {
		this.idProdotto = idProdotto;
	}

	public String getCodiceP() {
		return codiceP;
	}

	public void setCodiceP(String codiceP) {
		this.codiceP = codiceP;
	}

	public String getNomeP() {
		return nomeP;
	}

	public void setNomeP(String nomeP) {
		this.nomeP = nomeP;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return "Prodotto [idProdotto=" + idProdotto + ", codiceP=" + codiceP + ", nomeP=" + nomeP + ", prezzo=" + prezzo
				+ "]";
	}
	
}
