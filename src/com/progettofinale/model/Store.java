package com.progettofinale.model;

import java.util.ArrayList;

public class Store {
	private Integer idStore;
	private String nomeS;
	private String indirizzo;
	private ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();

	public Store() {

	}

	public Integer getIdStore() {
		return idStore;
	}

	public void setIdStore(Integer idStore) {
		this.idStore = idStore;
	}

	public String getNomeS() {
		return nomeS;
	}

	public void setNomeS(String nomeS) {
		this.nomeS = nomeS;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}

	public void addProdotto(Prodotto prodotto) {
		this.elencoProdotti.add(prodotto);
	}

	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}

	public String toStringHtmlTableEdit() {
		
//		ArrayList<Prodotto>= new ArrayList<Prodotto>();
		
		
		
		
		
		String risultato = "<tr>";
		risultato += "<td>" + this.idStore + "</d>";
		risultato += "<td>" + this.nomeS + "</td>";
		risultato += "<td>" + this.indirizzo + "</td>";

		risultato += "<td><a href='DeleteStore?idStore=" + this.idStore
				+ "'><i class=\"fas fa-trash\"></i></a></td>";
		risultato += "<td><a href='modificastoreadmin.jsp?idUtente=" + this.idStore
				+ "'><i class=\"fas fa-pencil\"></i></a></td>";
		
		risultato += "<td><a href='dettaglistore?idStore="+this.idStore+"'>"+"  premi per dettagli </a>" 
					+"</td> </tr>";
				
		return risultato;

	}

	@Override
	public String toString() {
		return "Store [idStore=" + idStore + ", nomeS=" + nomeS + ", indirizzo=" + indirizzo + ", elencoProdotti="
				+ elencoProdotti + "]";
	}
	
	
	
	

}
