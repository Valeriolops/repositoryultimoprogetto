

<%@page import="java.sql.SQLException"%>
<%@page import="com.progettofinale.dao.UtenteDao"%>
<%@page import="com.progettofinale.model.Utente"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

   
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>    <title>Elenco Prodotti</title>
</head>
<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">HOME</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="ADMINgestisciUtenti.jsp">Gestisci Utenti <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ADMINgestisciStore.jsp">Gestisci Store</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="ADMINgestisciOrdini.jsp">Gestisci Ordini</a>
            </li>
          </ul>
        </div>
      </nav>
    

    <div class="container">

        <div class="row mt-5">
            <div class="col">
                <h1>Lista Utenti</h1>
                <p>Di seguito puoi consultare tutta la lista degli utenti</p>
                <p> Per tornare alla pagina di benvenuto premi <a href="index.html">qui </a></p>
            </div>
        </div>
        

        
        <div class="row mt-5">
            <div class="col">

                <table class="table">
                    <thead>
                        <tr>
                            <th>idUtente</th>
                            <th>usernameU</th>
                            <th>pswU</th>
                            <th>indirizzo</th>
                          
                        </tr>
                    </thead>
                    <tbody>
                    
                    <%
                  
                    
                    
                    ArrayList<Utente> elenco_utenti = new ArrayList<Utente>();
        			UtenteDao uDao = new UtenteDao();
                
              try{  
            	  elenco_utenti= uDao.getAll();
              
              
              }catch(Exception e){
            		out.print("<script> alert('Errore di connessione al server!'); </script>");
              }
                    	for(int i=0; i<elenco_utenti.size(); i++){
                    		if(elenco_utenti.get(i).getTipologiaU().equals("UTENTE"))
                    		out.println(elenco_utenti.get(i).toStringHtmlTableEdit());
                    	}
                    %>
                    
                    </tbody>
                </table>

            </div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>