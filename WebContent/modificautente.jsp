<!DOCTYPE html>

<%@page import="com.progettofinale.model.Utente"%>
<%@page import="com.progettofinale.dao.UtenteDao"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>    <title>Elenco Prodotti</title>
</head>
<body>
  

    <div class="container">

        <div class="row mt-5">
            <div class="col">
                <h1>Modifica Utente</h1>
                <p>Di seguito puoi trovare i dettagli dell'utente, modifica i campi e clicca sul tasto <strong>SALVA</strong></p>
            </div>
        </div>
        
        <%
      Integer idUtenteDaEliminare = Integer.parseInt(request.getParameter("idUtente"));
		
		UtenteDao uDao = new UtenteDao();
		Utente uTemp = new Utente();
        	uTemp = uDao.getById(idUtenteDaEliminare);
        
        	
        %>
        
        <div class="row mt-5">
            <div class="col">

				<form name="modifica_utente" action="aggiornautente" method="POST">
				
					<div class="form-group">
			            <label for="input_id">id Utente</label>
			            <input    readonly="readonly"  type="number" class="form-control" name="input_id" value="<% out.print(uTemp.getIdUtente());  %>"   />
			        </div>
				
					<div class="form-group">
			            <label for="input_usernameU">username</label>
			            <input type="text" class="form-control" name="input_usernameU" value="<% out.print(uTemp.getUsernameU()); %>"/>
			        </div>
			       
					<div class="form-group">
			            <label for="input_paswU">password</label>
			            <input type="text" class="form-control" name="input_paswU" value="<% out.print(uTemp.getPswU()); %>"/>
			        </div>
			        
					<div class="form-group">
			            <label for="input_indirizzo">indirizzo</label>
			            <input type="text" class="form-control" name="input_indirizzo" value="<% out.print(uTemp.getIndirizzo()); %>"/>
			        </div>
			        
					
			        
			        <button type="submit" class="btn btn-success" name="btn_inserimento">SALVA</button>
		        </form>

            </div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
